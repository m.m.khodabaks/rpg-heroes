# RPG Heroes: Command Line Application
---

> **Simple, command-line based RPG**

## Intro
My first RPG developed in Java, using Maven and JUnit.

## The Heroes
The RPG has a variety to choose from four different Heroes:
- Mage
- Ranger
- Rogue
- Warrior

## Items
The heroes can equip weapons and armors.

Weapons:
+ Axe
+ Bow
+ Dagger
+ Hammer
+ Staff
+ Sword
+ Wand

Armors:
+ Cloth
+ Leather
+ Mail
+ Plate

Each character has the ability to equip certain items to increase their power.
Their powers are based on the following aspects:
- Strength
- Dexterity
- Intelligence
- Weapon damage
- Armor attribute

Each class of hero can only equip certain items:
+ **Mages** can equip **Staffs** and **Wands** for weapons, and **Cloth** for armor.
+ **Rangers** can equip **Bows** for weapon, and **Leather** and **Mail** for armor.
+ **Rogues** can equip **Daggers** and **Swords** for weapons, and **Leather** and **Mail** for armor.
+ **Warriors** can equip **Axes**, **Hammers** and **Swords** for weapons, and **Mail** and **Plate** for armor.

Each hero has **attributes**, their base attributes are automatically created.

They can also **level up**, on every level up their base attributes increase.

When a hero deals damage, their **Total Damage** is displayed, the DPS is calculated using the dps of their
equipped weapon and total attributes. Each class hase a **Main** attribute, that increase their damage.

The heroes Main attributes:
+ Mage: **Intelligence**
+ Ranger: **Dexterity**
+ Rogue: **Dexterity**
+ Warrior: **Strength**

## Unit testing
This project also includes unit testing for all major features. These test can be found in the **Tests** Directory.

## Installation
A few prerequisites that are necessary to install, and play the game:
- JDK 19
- Maven 4.0.0
- JUnit 5.8.1

## Getting Started
1. Clone the repository through command prompt, or other terminal:
```shell
git clone https://gitlab.com/m.m.khodabaks/rpg-heroes.git 
```

2. Change directory to the RPGHeroes folder:
```shell
cd RPGHeroes
```

3. Install the dependencies:
```shell
mvn clean install
```

# How to play?
## Command-Line Interface

Start the game by running the Main class of the project. Simply run the project, which will run Main first.
Users are greeted with a welcome message and a list of available commands.

List of available commands:
- `help`: Show a list of available commands.
- `create [classname] [name]`: Create a new hero with the given class and name.
- `levelup`: Increase the level of your hero by 1.
- `equipweapon [weapontype] [requiredlevel] [damage]`: Equip a weapon to your hero.
- `equiparmor [armortype] [requiredlevel] [slot] [strength] [dexterity] [intelligence]`: Equip armor to your hero.
- `display`: Display the stats of your hero.
- `quit`: Exit the game.

To execute a command, type it in the command prompt and press Enter. Use Ctrl + D or Ctrl + C to exit the game at any time.

## Example
- Create a new hero:
```shell
create warrior Kratos
```
- Level up your hero:
```shell
levelup
```
- Equip a weapon/armor to your hero:
```shell
equipweapon sword 1 10
equiparmor plate 1 chest 10 10 10
```
- Display your hero's stats:
```shell
display
```
