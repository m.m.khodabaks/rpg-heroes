import static org.junit.jupiter.api.Assertions.*;


import equipment.Item.Item;
import equipment.Armor.Armor;
import heroes.HeroAttribute;
import org.junit.jupiter.api.Test;

class ArmorTest {


    @Test
    public void create_Armor_shouldReturnCorrectName(){

        Armor armor = new Armor("SteinBjorn Helmet", 0, Armor.ArmorType.Plate, new HeroAttribute(0,0,0), Item.Slot.Head);

        assertEquals("SteinBjorn Helmet", armor.getName());
    }

    @Test
    public void create_Armor_shouldReturnCorrectRequiredLevel(){

        Armor armor = new Armor("SteinBjorn Helmet", 0, Armor.ArmorType.Plate, new HeroAttribute(0,0,0), Item.Slot.Head);

        assertEquals(0, armor.getRequiredLevel());

    }
    @Test
    public void create_Armor_shouldReturnCorrectSlots(){
        Armor head = new Armor("SteinBjorn Helmet", 0, Armor.ArmorType.Plate, new HeroAttribute(0,0,0), Item.Slot.Head);
        Armor body = new Armor("Bla", 0, Armor.ArmorType.Plate, new HeroAttribute(0,0,0), Item.Slot.Body);
        Armor legs = new Armor("Bla", 0, Armor.ArmorType.Plate, new HeroAttribute(0,0,0), Item.Slot.Legs);

        assertEquals(Item.Slot.Head, head.getSlot());
        assertEquals(Item.Slot.Body, body.getSlot());
        assertEquals(Item.Slot.Legs, legs.getSlot());
    }
    @Test
    public void create_Armor_shouldReturnCorrectArmorTypes(){
        Armor mail = new Armor("Bla", 0, Armor.ArmorType.Mail, new HeroAttribute(0,0,0), Item.Slot.Legs);
        Armor leather = new Armor("Bla", 0, Armor.ArmorType.Leather, new HeroAttribute(0,0,0), Item.Slot.Legs);
        Armor plate = new Armor("Bla", 0, Armor.ArmorType.Plate, new HeroAttribute(0,0,0), Item.Slot.Legs);
        Armor cloth = new Armor("Bla", 0, Armor.ArmorType.Cloth, new HeroAttribute(0,0,0), Item.Slot.Legs);

        assertEquals(Armor.ArmorType.Mail, mail.getArmorType());
        assertEquals(Armor.ArmorType.Leather, leather.getArmorType());
        assertEquals(Armor.ArmorType.Plate, plate.getArmorType());
        assertEquals(Armor.ArmorType.Cloth, cloth.getArmorType());
    }

    @Test
    public void create_Armor_shouldReturnCorrectAttributes(){
        Armor armor = new Armor("Bla", 99, Armor.ArmorType.Plate, new HeroAttribute(11, 11, 11), Item.Slot.Body);

        assertEquals(11, armor.getArmorAttribute().getStrength());
        assertEquals(11, armor.getArmorAttribute().getDexterity());
        assertEquals(11, armor.getArmorAttribute().getIntelligence());
    }


}