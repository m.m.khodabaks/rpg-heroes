import static org.junit.jupiter.api.Assertions.*;


import equipment.Item.Item;
import equipment.Weapon.Weapon;
import org.junit.jupiter.api.Test;

class WeaponTest {


    @Test
    public void create_Weapon_shouldReturnCorrectName(){

        Weapon weapon = new Weapon("Leviathan Axe", 0, Weapon.WeaponType.Axe, 0);

        // Arrange
        String expected = "Leviathan Axe";
        // Act
        String actual = weapon.getName();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void create_Weapon_shouldReturnCorrectRequiredLevel(){

        Weapon weapon = new Weapon("Leviathan Axe", 0, Weapon.WeaponType.Axe, 0);

        int expected = 0;

        int actual = weapon.getRequiredLevel();

        assertEquals(expected, actual);
    }
    @Test
    public void create_Weapon_shouldReturnCorrectSlot(){
        Weapon weapon = new Weapon("Leviathan Axe", 0, Weapon.WeaponType.Axe, 0);

        assertEquals(Item.Slot.Weapon, weapon.getSlot());
    }
    @Test
    public void create_Weapon_shouldReturnCorrectWeaponType(){
        Weapon weapon = new Weapon("Leviathan Axe", 0, Weapon.WeaponType.Axe, 0);

        assertEquals(Weapon.WeaponType.Axe, weapon.getWeaponType());
    }

    @Test
    public void create_Weapon_shouldReturnCorrectDamage(){
        Weapon weapon = new Weapon("Leviathan Axe", 0, Weapon.WeaponType.Axe, 0);

        assertEquals(0, weapon.getWeaponDamage());

    }


}