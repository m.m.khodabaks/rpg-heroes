import equipment.Armor.Armor;
import equipment.Item.Item;
import equipment.Weapon.Weapon;
import exceptions.InvalidArmorException;
import exceptions.InvalidLevelException;
import exceptions.InvalidWeaponException;
import heroes.HeroAttribute;
import heroes.Mage;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

class MageTest {

    @Test
    public void create_Mage_ShouldReturnCorrectNameLevelAndAttributes() {

        Mage mage = new Mage("Merlin");

        // arrange
        String expectedName = "Merlin";
        // act
        String actualName = mage.getName();
        // assert
        assertEquals(expectedName, actualName);

        // arrange
        int expectedLevel = 1;
        // act
        int actualLevel = mage.getLevel();

        assertEquals(expectedLevel, actualLevel);

        // arrange
        int expectedStrength = 1;
        // act
        int actualStrength = mage.totalAttributes().getStrength();
        // assert
        assertEquals(expectedStrength, actualStrength);

        // arrange
        int expectedDexterity = 1;
        // act
        int actualDexterity = mage.totalAttributes().getDexterity();
        // assert
        assertEquals(expectedDexterity, actualDexterity);

        // arrange
        int expectedIntelligence = 8;
        // act
        int actualIntelligence = mage.totalAttributes().getIntelligence();
        // assert
        assertEquals(expectedIntelligence, actualIntelligence);
    }

    public void levelUp_Mage_ShouldReturnCorrectIncrements() {
        Mage mage = new Mage("Merlin");

        // arrange
        int expected = 2;
        // act
        mage.levelUp();
        int actual = mage.getLevel();
        // assert
        assertEquals(expected, actual);

        // Arrange
        int expectedStrength = 2;
        // Act
        int actualStrength = mage.totalAttributes().getStrength();
        // assert
        assertEquals(expectedStrength, actualStrength);

        // arrange
        int expectedDexterity = 2;
        // act
        int actualDexterity = mage.totalAttributes().getDexterity();
        // assert
        assertEquals(expectedDexterity, actualDexterity);

        // arrange
        int expectedIntelligence = 8;
        // act
        int actualIntelligence = mage.totalAttributes().getIntelligence();
        // assert
        assertEquals(expectedIntelligence, actualIntelligence);
    }

    @Test
    public void equip_Weapon_ShouldThrowWeaponException() throws InvalidLevelException {
        Mage mage = new Mage("x");
        Weapon weapon = new Weapon("x", 1, Weapon.WeaponType.Sword, 0);

        try {
            mage.equipWeapon(weapon);
            fail("It should fail at this point");
        } catch (InvalidWeaponException e) {

        }
    }

    @Test
    public void equip_Weapon_ShouldThrowLevelException() throws InvalidWeaponException {
        Mage mage = new Mage("x");
        Weapon weapon = new Weapon("x", 99, Weapon.WeaponType.Staff, 0);

        try {
            mage.equipWeapon(weapon);
            fail("It should fail at this point");
        } catch (InvalidLevelException e) {
        }
    }

    @Test
    public void equip_Armor_ShouldThrowArmorException() throws InvalidLevelException {
        Mage mage = new Mage("x");
        Armor armor = new Armor("Bla", 0, Armor.ArmorType.Plate, new HeroAttribute(0, 0, 0), Item.Slot.Legs);

        try {
            mage.equipArmor(armor);
            fail("It should fail at this point");
        } catch (InvalidArmorException e) {

        }
    }

    @Test
    public void equip_Armor_ShouldThrowLevelException() throws InvalidArmorException {
        Mage mage = new Mage("x");
        Armor armor = new Armor("Bla", 10, Armor.ArmorType.Cloth, new HeroAttribute(0, 0, 0), Item.Slot.Legs);

        try {
            mage.equipArmor(armor);
            fail("It should fail at this point");
        } catch (InvalidLevelException e) {
        }
    }

    @Test
    public void equip_OnePieceOfArmor_ShouldReturnCorrectAttributeCalculations() throws InvalidArmorException, InvalidLevelException {
        Mage mage = new Mage("x");
        Armor cloth = new Armor("XX", 1, Armor.ArmorType.Cloth, new HeroAttribute(7, 8, 9), Item.Slot.Body);

        mage.equipArmor(cloth);
        assertEquals(8, mage.totalAttributes().getStrength());
        assertEquals(9, mage.totalAttributes().getDexterity());
        assertEquals(17, mage.totalAttributes().getIntelligence());

    }

    @Test
    public void equip_TwoPiecesOfArmor_ShouldReturnCorrectAttributeCalculations() throws InvalidArmorException, InvalidLevelException {
        Mage mage = new Mage("x");
        Armor cloth = new Armor("XX", 1, Armor.ArmorType.Cloth, new HeroAttribute(7, 8, 9), Item.Slot.Body);
        Armor cloth2 = new Armor("XXX", 1, Armor.ArmorType.Cloth, new HeroAttribute(7, 8, 9), Item.Slot.Legs);

        mage.equipArmor(cloth);
        mage.equipArmor(cloth2);
        assertEquals(15, mage.totalAttributes().getStrength());
        assertEquals(17, mage.totalAttributes().getDexterity());
        assertEquals(26, mage.totalAttributes().getIntelligence());

    }

    @Test
    public void equip_ReplacedPieceOfArmor_ShouldReturnCorrectAttributeCalculations() throws InvalidArmorException, InvalidLevelException {
        Mage mage = new Mage("x");
        Armor piece = new Armor("piece", 1, Armor.ArmorType.Cloth, new HeroAttribute(33, 33, 33), Item.Slot.Body);

        mage.equipArmor(piece);
        assertEquals(34, mage.totalAttributes().getStrength());
        assertEquals(34, mage.totalAttributes().getDexterity());
        assertEquals(41, mage.totalAttributes().getIntelligence());

        Armor replacedPiece = new Armor("replaced piece", 1, Armor.ArmorType.Cloth, new HeroAttribute(1, 1, 1), Item.Slot.Body);
        mage.equipArmor(replacedPiece);
        assertEquals(2, mage.totalAttributes().getStrength());
        assertEquals(2, mage.totalAttributes().getDexterity());
        assertEquals(9, mage.totalAttributes().getIntelligence());
    }

    @Test
    public void equip_Weapon_ShouldReturnCorrectDamage() throws InvalidLevelException, InvalidWeaponException {
        Mage mage = new Mage("X");
        Weapon staff = new Weapon("Staff", 0, Weapon.WeaponType.Staff, 325);

        mage.equipWeapon(staff);

        assertEquals(325 * (1 + (8 / 100)), staff.getWeaponDamage());
    }

    @Test
    public void equip_NoWeapon_ShouldReturnCorrectDamage() {
        Mage mage = new Mage("X");

        assertEquals((1 * (1 + (8.00 / 100))), mage.calculateDamage());
    }

    @Test
    public void equip_ReplacedWeapon_ShouldReturnCorrectDamage() throws InvalidLevelException, InvalidWeaponException {
        Mage mage = new Mage("X");
        Weapon staff = new Weapon("Staff", 0, Weapon.WeaponType.Staff, 43404);

        mage.equipWeapon(staff);
        assertEquals(43404, staff.getWeaponDamage());

        Weapon replacedStaff = new Weapon("replaced Staff", 1, Weapon.WeaponType.Staff, 6);
        mage.equipWeapon(replacedStaff);
        assertEquals(6, replacedStaff.getWeaponDamage());
    }

    @Test
    public void equip_ArmorAndWeapon_ShouldReturnCorrectDamage() throws InvalidWeaponException, InvalidArmorException, InvalidLevelException {
        Mage mage = new Mage("X");
        Weapon staff = new Weapon("XX", 0, Weapon.WeaponType.Staff, 5000);
        Armor head = new Armor("SteinBjorn Helmet", 0, Armor.ArmorType.Cloth, new HeroAttribute(5, 5, 8), Item.Slot.Head);
        Armor body = new Armor("Bla", 0, Armor.ArmorType.Cloth, new HeroAttribute(1, 1, 50), Item.Slot.Body);
        Armor legs = new Armor("Bla", 0, Armor.ArmorType.Cloth, new HeroAttribute(3, 3, 500), Item.Slot.Legs);

        mage.equipWeapon(staff);
        mage.equipArmor(head);
        mage.equipArmor(body);
        mage.equipArmor(legs);

        assertEquals((5000 * (1 + (8 / 100))), staff.getWeaponDamage());
        assertEquals(10, mage.totalAttributes().getStrength());
        assertEquals(10, mage.totalAttributes().getDexterity());
        assertEquals(566, mage.totalAttributes().getIntelligence());
    }

    @Test
    public void show_Display_ShouldReturnCorrectOutput() {
        Mage mage = new Mage("Merlin");
        String expected = "Name: Merlin\n" +
                "Class: Mage\n" +
                "Level: 1\n" +
                "Total Strength: 1\n" +
                "Total Dexterity: 1\n" +
                "Total Intelligence: 8\n" +
                "Total Damage: 1.08\n\n";
        assertEquals(expected, mage.display());
    }
}