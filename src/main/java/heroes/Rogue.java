/**
 * Class representing a Rogue in the Hero RPG
 *
 * @author Masood Khodabaks
 * @version 1.0
 * @since 13 January 2023
 */
package heroes;

import equipment.Armor.Armor;
import equipment.Item.Item;
import equipment.Weapon.Weapon;

import java.util.ArrayList;

public class Rogue extends Hero{
    /*
     * Base strength of Rogue
     */
    public static final int START_STRENGTH = 2;
    /*
     * Base dexterity of Rogue
     */
    public static final int START_DEXTERITY = 6;
    /*
     * Base intelligence of Rogue
     */
    public static final int START_INTELLIGENCE = 1;

    /*
     * Increment of the strength, dexterity, and intelligence attribute, when Rogue gains 1 level
     */
    private static final int STRENGTH_INCREMENT_PER_LEVEL = 1;
    private static final int DEXTERITY_INCREMENT_PER_LEVEL = 4;
    private static final int INTELLIGENCE_INCREMENT_PER_LEVEL = 1;

    /**
     * The total increase of the attributes for Rogue, using a {@link HeroAttribute} type object
     */
    private static final HeroAttribute INCREASE_HERO_ATTRIBUTES = new HeroAttribute(STRENGTH_INCREMENT_PER_LEVEL,
            DEXTERITY_INCREMENT_PER_LEVEL,
            INTELLIGENCE_INCREMENT_PER_LEVEL);

    /**
     * Constructor for the {@link Rogue} class
     *
     * @param name The name of Rogue
     * Super - Adds the name from the abstract {@link Hero} class
     * Super - Changes the {@link HeroAttribute} to the Rogue's specific starting attributes
     * validWeaponTypes checks if Rogue equips the appropriate weapons (Daggers, Swords), throws InvalidWeaponException
     * ValidArmorTypes checks if Rogue equips the appropriate armorType(Leather, Mail), throws InvalidArmorException.
     */
    public Rogue(String name){
        super(name, new HeroAttribute(START_STRENGTH, START_DEXTERITY, START_INTELLIGENCE));
        this.validWeaponTypes = new ArrayList<>();
        this.validWeaponTypes.add(Weapon.WeaponType.Dagger);
        this.validWeaponTypes.add(Weapon.WeaponType.Sword);

        this.validArmorTypes = new ArrayList<>();
        this.validArmorTypes.add(Armor.ArmorType.Mail);
        this.validArmorTypes.add(Armor.ArmorType.Leather);
    }

    /**
     * Increase level of Rogue by 1, and increase its attributes accordingly
     */
    @Override
    public void levelUp(){
        super.levelUp();
        this.levelAttributes.increase(INCREASE_HERO_ATTRIBUTES);
    }

    /**
     * Returns Rogue's dealing damage
     * @return Double representing Rogue weapon damage
     * If Rogue has no weapon equipped, sets default damage on 1 (bare-handed fighting)
     * Damaging attribute for Rogue is dexterity, therefore added to the weapon damage
     */
    @Override
    public double calculateDamage() {
        Weapon equippedWeapon = (Weapon) equipment.get(Item.Slot.Weapon);
        double weaponDamage = equippedWeapon != null ? equippedWeapon.getWeaponDamage() : 1;

        double damageAttribute = totalAttributes().getDexterity();
        return weaponDamage * (1 + (damageAttribute / 100));
    }
}
