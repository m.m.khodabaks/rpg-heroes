/**
 * Class representing the Hero attributes in the Hero RPG
 *
 * @author Masood Khodabaks
 * @version 1.0
 * @since 13 January 2023
 */
package heroes;

public class HeroAttribute {
    private int strength;
    private int dexterity;
    private int intelligence;

    /**
     * Constructor for the HeroAttribute class.
     *
     * @param strength The strength attribute of hero
     * @param dexterity The dexterity attribute of hero
     * @param intelligence The intelligence attribute of hero
     */
    public HeroAttribute(int strength, int dexterity, int intelligence){
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    /**
     * Getter that returns that strength attribute of hero
     * @return integer representing strength attribute
     */
    public int getStrength(){
        return strength;
    }
    /**
     * Getter that returns that dexterity attribute of hero
     * @return integer representing dexterity attribute
     */
    public int getDexterity() {
        return dexterity;
    }
    /**
     * Getter that returns that intelligence attribute of hero
     * @return integer representing intelligence attribute
     */
    public int getIntelligence() {
        return intelligence;
    }

    /**
     * Increases the attributes of the hero by the attributes of the armorAttribute
     * @param otherAttributes a {@link HeroAttribute} type object that stores the attributes of the armor
     *                        and add it to the hero's current attributes
     */
    public void increase(HeroAttribute otherAttributes){
        this.strength += otherAttributes.strength;
        this.dexterity += otherAttributes.dexterity;
        this.intelligence += otherAttributes.intelligence;
    }
}
