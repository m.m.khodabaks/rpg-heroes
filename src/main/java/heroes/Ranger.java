/**
 * Class representing a Ranger in the Hero RPG
 *
 * @author Masood Khodabaks
 * @version 1.0
 * @since 13 January 2023
 */
package heroes;

import equipment.Item.Item;
import equipment.Armor.Armor;
import equipment.Weapon.Weapon;
import java.util.ArrayList;

public class Ranger extends Hero {
    /*
     * Base strength of Ranger
     */
    public static final int START_STRENGTH = 1;
    /*
     * Base dexterity of Ranger
     */
    public static final int START_DEXTERITY = 7;
    /*
     * Base intelligence of Ranger
     */
    public static final int START_INTELLIGENCE = 1;

    /*
     * Increment of the strength, dexterity, and intelligence attribute, when Ranger gains 1 level
     */
    private static final int STRENGTH_INCREMENT_PER_LEVEL = 1;
    private static final int DEXTERITY_INCREMENT_PER_LEVEL = 5;
    private static final int INTELLIGENCE_INCREMENT_PER_LEVEL = 1;

    /**
     * The total increase of the attributes for Ranger, using a {@link HeroAttribute} type object
     */
    private static final HeroAttribute INCREASE_HERO_ATTRIBUTES = new HeroAttribute(STRENGTH_INCREMENT_PER_LEVEL,
            DEXTERITY_INCREMENT_PER_LEVEL,
            INTELLIGENCE_INCREMENT_PER_LEVEL);

    /**
     * Constructor for the {@link Ranger} class
     *
     * @param name The name of Mage
     * Super - Adds the name from the abstract {@link Hero} class
     * Super - Changes the {@link HeroAttribute} to the Ranger's specific starting attributes
     * validWeaponTypes checks if Ranger equips the appropriate weapons (Bow), throws InvalidWeaponException
     * ValidArmorTypes checks if Ranger equips the appropriate armorType(Leather, Mail), throws InvalidArmorException.
     */
    public Ranger(String name){
        super(name, new HeroAttribute(START_STRENGTH, START_DEXTERITY, START_INTELLIGENCE));
        this.validWeaponTypes = new ArrayList<>();
        this.validWeaponTypes.add(Weapon.WeaponType.Bow);

        this.validArmorTypes = new ArrayList<>();
        this.validArmorTypes.add(Armor.ArmorType.Mail);
        this.validArmorTypes.add(Armor.ArmorType.Leather);
    }

    /**
     * Increase level of Ranger by 1, and increase its attributes accordingly
     */
    @Override
    public void levelUp(){
        super.levelUp();
        this.levelAttributes.increase(INCREASE_HERO_ATTRIBUTES);
    }

    /**
     * Returns Ranger's dealing damage
     * @return Double representing Ranger weapon damage
     * If Ranger has no weapon equipped, sets default damage on 1 (bare-handed fighting)
     * Damaging attribute for Ranger is dexterity, therefore added to the weapon damage
     */
    @Override
    public double calculateDamage() {
        Weapon equippedWeapon = (Weapon) equipment.get(Item.Slot.Weapon);
        double weaponDamage = equippedWeapon != null ? equippedWeapon.getWeaponDamage() : 1;

        double damageAttribute = totalAttributes().getDexterity();
        return weaponDamage * (1 + (damageAttribute / 100));
    }
}
