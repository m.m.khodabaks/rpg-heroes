/**
 * Class representing a Warrior in the Hero RPG
 *
 * @author Masood Khodabaks
 * @version 1.0
 * @since 13 January 2023
 */
package heroes;

import equipment.Armor.Armor;
import equipment.Item.Item;
import equipment.Weapon.Weapon;

import java.util.ArrayList;

public class Warrior extends Hero{
    /*
     * Base strength of Warrior
     */
    public static final int START_STRENGTH = 5;
    /*
     * Base dexterity of Warrior
     */
    public static final int START_DEXTERITY = 2;
    /*
     * Base intelligence of Warrior
     */
    public static final int START_INTELLIGENCE = 1;

    /*
     * Increment of the strength, dexterity, and intelligence attribute, when Warrior gains 1 level
     */
    private static final int STRENGTH_INCREMENT_PER_LEVEL = 3;
    private static final int DEXTERITY_INCREMENT_PER_LEVEL = 2;
    private static final int INTELLIGENCE_INCREMENT_PER_LEVEL = 1;

    /**
     * The total increase of the attributes for Warrior, using a {@link HeroAttribute} type object
     */
    private static final HeroAttribute INCREASE_HERO_ATTRIBUTES = new HeroAttribute(STRENGTH_INCREMENT_PER_LEVEL,
            DEXTERITY_INCREMENT_PER_LEVEL,
            INTELLIGENCE_INCREMENT_PER_LEVEL);

    /**
     * Constructor for the {@link Warrior} class
     *
     * @param name The name of Warrior
     * Super - Adds the name from the abstract {@link Hero} class
     * Super - Changes the {@link HeroAttribute} to the Warrior's specific starting attributes
     * validWeaponTypes checks if Warrior equips the appropriate weapons (Axe, Dagger, Sword), throws InvalidWeaponException
     * ValidArmorTypes checks if Rogue equips the appropriate armorType(Mail, Plate), throws InvalidArmorException.
     */
    public Warrior(String name){
        super(name, new HeroAttribute(START_STRENGTH, START_DEXTERITY, START_INTELLIGENCE));
        this.validWeaponTypes = new ArrayList<>();
        this.validWeaponTypes.add(Weapon.WeaponType.Axe);
        this.validWeaponTypes.add(Weapon.WeaponType.Hammer);
        this.validWeaponTypes.add(Weapon.WeaponType.Sword);

        this.validArmorTypes = new ArrayList<>();
        this.validArmorTypes.add(Armor.ArmorType.Mail);
        this.validArmorTypes.add(Armor.ArmorType.Plate);
    }

    /**
     * Increase level of Warrior by 1, and increase its attributes accordingly
     */
    @Override
    public void levelUp(){
        super.levelUp();
        this.levelAttributes.increase(INCREASE_HERO_ATTRIBUTES);
    }

    /**
     * Returns Warrior's dealing damage
     * @return Double representing Warrior weapon damage
     * If Warrior has no weapon equipped, sets default damage on 1 (bare-handed fighting)
     * Damaging attribute for Rogue is strength, therefore added to the weapon damage
     */
    @Override
    public double calculateDamage() {
        Weapon equippedWeapon = (Weapon) equipment.get(Item.Slot.Weapon);
        double weaponDamage = equippedWeapon != null ? equippedWeapon.getWeaponDamage() : 1;

        double damageAttribute = totalAttributes().getStrength();
        return weaponDamage * (1 + (damageAttribute / 100));
    }
}
