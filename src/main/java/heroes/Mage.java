/**
 * Class representing a Mage in the Hero RPG
 *
 * @author Masood Khodabaks
 * @version 1.0
 * @since 13 January 2023
 */
package heroes;

import equipment.Armor.Armor;
import equipment.Item.Item;
import equipment.Weapon.Weapon;

import java.util.ArrayList;


public class Mage extends Hero {
    /*
    * Base strength of Mage
     */
    private static final int START_STRENGTH = 1;
    /*
     * Base dexterity of Mage
     */
    private static final int START_DEXTERITY = 1;
    /*
     * Base intelligence of Mage
     */
    private static final int START_INTELLIGENCE = 8;
    /*
     * Increment of the strength, dexterity, and intelligence attribute, when Mage gains 1 level
     */
    private static final int STRENGTH_INCREMENT_PER_LEVEL = 1;
    private static final int DEXTERITY_INCREMENT_PER_LEVEL = 1;
    private static final int INTELLIGENCE_INCREMENT_PER_LEVEL = 5;

    /**
     * The total increase of the attributes for Mage, using a {@link HeroAttribute} type object
     */

    private static final HeroAttribute INCREASE_HERO_ATTRIBUTES = new HeroAttribute(STRENGTH_INCREMENT_PER_LEVEL,
                                                                                    DEXTERITY_INCREMENT_PER_LEVEL,
                                                                                    INTELLIGENCE_INCREMENT_PER_LEVEL);
    /**
     * Constructor for the {@link Mage} class
     *
     * @param name The name of Mage
     * Super - Adds the name from the abstract {@link Hero} class
     * Super - Changes the {@link HeroAttribute} to the Mage's specific starting attributes
     * validWeaponTypes checks if Mage equips the appropriate weapons (Staff, Wand), throws InvalidWeaponException
     * ValidArmorTypes checks if Mage equips the appropriate armorType(Cloth), throws InvalidArmorException.
     */
    public Mage(String name){
        super(name, new HeroAttribute(START_STRENGTH, START_DEXTERITY, START_INTELLIGENCE));
        this.validWeaponTypes = new ArrayList<>();
        this.validWeaponTypes.add(Weapon.WeaponType.Staff);
        this.validWeaponTypes.add(Weapon.WeaponType.Wand);

        this.validArmorTypes = new ArrayList<>();
        this.validArmorTypes.add(Armor.ArmorType.Cloth);
    }

    /**
     * Increase level of Mage by 1, and increase its attributes accordingly
     */
    @Override
    public void levelUp(){
        super.levelUp();
        this.levelAttributes.increase(INCREASE_HERO_ATTRIBUTES);
    }

    /**
     * Returns Mage's dealing damage
     * @return Double representing Mage weapon damage
     * If Mage has no weapon equipped, sets default damage on 1 (bare-handed fighting)
     * Damaging attribute for Mage is intelligence, therefore added to the weapon damage
     */
    @Override
    public double calculateDamage(){
        Weapon equippedWeapon = (Weapon) equipment.get(Item.Slot.Weapon);
        double weaponDamage = equippedWeapon != null ? equippedWeapon.getWeaponDamage() : 1;

        double damageAttribute = totalAttributes().getIntelligence();
        return weaponDamage * (1 + (damageAttribute / 100));
    }

}

