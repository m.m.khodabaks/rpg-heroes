import heroes.*;
import java.io.IOException;
import com.github.lalyos.jfiglet.FigletFont;
import org.jline.reader.LineReader;
import org.jline.reader.LineReaderBuilder;
import org.jline.reader.UserInterruptException;
import org.jline.reader.EndOfFileException;
import org.jline.terminal.Terminal;
import org.jline.terminal.TerminalBuilder;
import java.io.PrintWriter;


public class Main {
    public static void main(String[] args) throws IOException{

        Terminal terminal = TerminalBuilder.builder()
            .system(true)
            .build();
        LineReader reader = LineReaderBuilder.builder()
                .terminal(terminal)
            .build();
        PrintWriter out = terminal.writer();

        String asciiArt = FigletFont.convertOneLine("RPG Heroes");
        out.printf("%s\n", asciiArt);

        out.printf("Welcome to Heroes RPG!\n");
        out.printf("Type 'help' to see a list of available commands!\n");

        Hero hero = null;

        while (true) {
            String command;
            try {
                command = reader.readLine("> ").trim().toLowerCase();
            } catch (UserInterruptException | EndOfFileException e) {
                // Exit the game with Ctrl + D or Ctrl + C
                break;
            }

            if (command.equals("quit")) {
                break;
            } else if (command.equals("help")) {
                out.printf("Available commands:\n");
                out.printf("create [classname] [name] - Create a new hero with a given class and name\n");
                out.printf("levelup - Increase the level of your hero by 1\n");
                out.printf("equipweapon [weapontype] [requiredlevel] [damage] - Equip a weapon to your hero\n");
                out.printf("equiparmor [armortype] [requiredlevel] [slot] [strength] [dexterity] [intelligence] - Equip armor to your hero\n");
                out.printf("display - Display the stats of your hero\n");
                out.printf("help - Show available commands\n");
                out.printf("quit - Exit the game\n");
            } else if (command.startsWith("create ")) {
                String[] parts = command.split(" ");
                if (parts.length == 3) {
                    String className = parts[1];
                    String name = parts[2];
                    if (className.equalsIgnoreCase("Warrior")) {
                        hero = new Warrior(name);
                    } else if (className.equalsIgnoreCase("Mage")) {
                        hero = new Mage(name);
                    } else if (className.equalsIgnoreCase("Rogue")) {
                        hero = new Rogue(name);
                    } else if (className.equalsIgnoreCase("Ranger")) {
                        hero = new Ranger(name);
                    }else {
                        out.printf("Invalid class name. Available classes: Warrior, Mage, Rogue, and Ranger\n");
                        continue;
                    }
                    out.printf("Hero created: %s\n", hero.display());
                } else {
                    out.printf("Invalid command. Usage: create [classname] [name]\n");
                }
            } else if (command.equals("levelup")) {
                if (hero != null) {
                    hero.levelUp();
                    out.printf("Hero level increased to %d\n", hero.getLevel());
                } else {
                    out.printf("No hero created. Use 'create' command to create a hero.\n");
                }
            } else if (command.startsWith("equipweapon ")) {
                if (hero == null) {
                    out.printf("No hero created. Use 'create' command to create a hero.\n");
                    continue;
                }

                // Parse weapon details and equip weapon
            } else if (command.startsWith("equiparmor ")) {
                if (hero == null) {
                    out.printf("No hero created. Use 'create' command to create a hero.\n");
                    continue;
                }

                // Parse armor details and equip armor
            } else if (command.equals("display")) {
                if (hero != null) {
                    out.printf("%s\n", hero.display());
                } else {
                    out.printf("No hero created. Use 'create' command to create a hero.\n");
                }
            } else {
                out.printf("Invalid command. Type 'help' to see a list of available commands!\n");
            }
        }
        out.printf("Thank you for playing Heroes RPG! Goodbye!\n");
    }
}
