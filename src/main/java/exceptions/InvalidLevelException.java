/**
 * Exception thrown when required level for a weapon, or armor, is not met
 *
 * @author Masood Khodabaks
 * @version 1.0
 * @since 13 January 2023
 */
package exceptions;

public class InvalidLevelException extends Exception{
    /**
     * Constructor for InvalidLevelException.
     *
     * @param message The message to be displayed when the exception is thrown.
     */
    public InvalidLevelException(String message){
        super(message);
    }
}
