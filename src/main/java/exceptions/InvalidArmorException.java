/**
 * Exception thrown when an invalid armor is equipped
 * @author Masood Khodabaks
 * @version 1.0
 * @since 13 January 2023
 */
package exceptions;

public class InvalidArmorException extends Exception{
    /**
     * Constructor for InvalidArmorException
     * @param message The message to be displayed when exception is thrown
     */
   public InvalidArmorException(String message){
        super(message);
    }
}
