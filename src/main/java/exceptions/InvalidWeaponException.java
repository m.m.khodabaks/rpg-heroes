/**
 * Exception thrown when invalid weapon is equipped
 *
 * @author Masood Khodabaks
 * @version 1.0
 * @since 13 January 2023
 */
package exceptions;

public class InvalidWeaponException extends Exception {
    /**
     * Constructor for InvalidWeaponException.
     *
     * @param message The message to be displayed when the exception is thrown.
     */
    public InvalidWeaponException(String message){
        super(message);
    }

}
