/**
 * Class representing the weapons in the Hero RPG
 * @name Masood Khodabaks
 * @version 1.0
 * @since 13 January 2023
 */
package equipment.Weapon;

import equipment.Item.Item;

public class Weapon extends Item {
    /**
     * Enumeration representing the different types of weapons.
     */
    public enum WeaponType {
        Axe, Bow, Dagger, Hammer, Staff, Sword, Wand
    }

    private WeaponType weaponType;
    private int weaponDamage;

    /**
     * Constructor for the Weapon class
     *
     * @param name The name of a weapon
     * @param requiredLevel The level required to use a weapon
     * @param weaponType The type of weapon
     * @param weaponDamage The damage dealt by a weapon
     */
    public Weapon(String name, int requiredLevel, WeaponType weaponType, int weaponDamage){
        super(name, requiredLevel, Slot.Weapon);
        this.weaponType = weaponType;
        this.weaponDamage = weaponDamage;

    }

    /**
     * Getter that returns the type of weapon
     *
     * @return A WeaponType enumeration value representing the type of weapon
     */
    public WeaponType getWeaponType() {
        return weaponType;
    }

    /**
     * Getter that returns weapon damage
     *
     * @return An integer representing weapon damage
     */
    public int getWeaponDamage() {
        return weaponDamage;
    }


}
