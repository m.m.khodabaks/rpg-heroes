/**
 * Class {@link equipment.Armor.Armor} represents the types of armor a Hero equips
 *
 * @author Masood Khodabaks
 * @version 1.0
 * @since 13 January 2023
 */
package equipment.Armor;

import equipment.Item.Item;
import heroes.HeroAttribute;


public class Armor extends Item {

    /**
     * Enumeration class representing the types of armor
     */
    public enum ArmorType {
        Cloth, Leather, Mail, Plate
    }

    private final ArmorType armorType;
    private final HeroAttribute armorAttribute;


    /**
     * Constructor for {@link Armor} class
     * @param name Name of armor
     * @param requiredLevel Level required to wear the armor
     * @param armorType Type of armor
     * @param armorAttribute The attribute that contributes to the damage attribute of a Hero
     * @param slot Equipment slot to equip the armor to
     */
    public Armor(String name, int requiredLevel, ArmorType armorType, HeroAttribute armorAttribute, Slot slot) {
        super(name, requiredLevel, slot);
        this.armorType = armorType;
        this.armorAttribute = armorAttribute;

    }

    /**
     * Getter that returns the current armor type equipped
     * @return An ArmorType enumeration value representing the type of armor
     */
    public ArmorType getArmorType() {
        return armorType;
    }

    /**
     * Getter that returns the Hero attribute that the armor carries
     * @return HeroAttribute enumeration value representing the attribute the armor carries.
     */
    public HeroAttribute getArmorAttribute() {
        return armorAttribute;
    }
}
