/**
 * Abstract class representing an Item for a Hero in the RPG
 *
 * @name Masood Khodabaks
 * @version 1.0
 * @since 13 January 2023
 */
package equipment.Item;

public abstract class Item {
    private String name;
    private int requiredLevel;
    private Slot slot;

    /**
     * Constructor for the Item class
     *
     * @param name The name of item
     * @param requiredLevel The level required to use item
     * @param slot The equipment slot the item is equipped to.
     */
    public Item(String name, int requiredLevel, Slot slot){
        this.name = name;
        this.requiredLevel = requiredLevel;
        this.slot = slot;
    }

    /**
     * Getter that returns the name of an item
     *
     * @return A integer representing the required level.
     */
    public String getName(){
        return name;
    }

    /**
     * Getter that returns the required level to use an item
     *
     * @return A string representing the name of item.
     */
    public int getRequiredLevel(){
        return requiredLevel;
    }

    /**
     * Getter that returns {@link Slot} type object
     *
     * @return A Slot enumeration value representing the equipment slot
     */
    public Slot getSlot(){
        return slot;
    }

    public enum Slot {
        Weapon, Head, Body, Legs
    }
}
